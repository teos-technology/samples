﻿using System;

namespace DataAccess.Model
{
	public abstract class EntityBase
	{
		public Guid Id;

		public override bool Equals(object obj)
		{
			EntityBase other = obj as EntityBase;

			return (other != null) && (this.Id == other.Id);
		}
	}
}