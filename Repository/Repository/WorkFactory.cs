﻿using DataAccess.Interface;

namespace DataAccess
{
	public class WorkFactory : IWorkFactory
	{
		public IUnitOfWork CreateUnitOfWork()
		{
			return new UnitOfWork();
		}
	}
}
