﻿using DataAccess.Interface;
using DataAccess.Model;
using System.Data.Entity;

namespace DataAccess
{
	public class StoreContext : DbContext, IStoreContext
	{
		public StoreContext() : base("MyConnectionString")
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Order> Orders { get; set; }
	}
}