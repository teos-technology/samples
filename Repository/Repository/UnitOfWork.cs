﻿using DataAccess.Interface;
using DataAccess.Model;

namespace DataAccess
{
	public class UnitOfWork : IUnitOfWork
	{
		private IStoreContext _context;

		public UnitOfWork()
		{
			_context = new StoreContext();
		}

		public IRepository<TEntity> GetRepository<TEntity>() 
			where TEntity : EntityBase
		{
			return new Repository<TEntity>(_context);
		}

		public int SaveChanges()
		{
			return _context.SaveChanges();
		}

		public void Dispose()
		{
			_context.Dispose();
		}
	}
}