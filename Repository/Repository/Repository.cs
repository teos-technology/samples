﻿using DataAccess.Interface;
using DataAccess.Model;
using System;
using System.Data.Entity;

namespace DataAccess
{
	public class Repository<TEntity> : IRepository<TEntity> 
		where TEntity : EntityBase
	{
		private IStoreContext _context;
		private DbSet<TEntity> _entities;

		internal Repository(IStoreContext context)
		{
			_context = context;
			_entities = _context.Set<TEntity>();
		}

		public void Create(TEntity entity)
		{
			_context.Entry<TEntity>(entity).State = 
				EntityState.Added;
			_entities.Add(entity);
		}

		public TEntity Retrieve(Guid id)
		{
			return _entities.Find(id);
		}

		public void Update(TEntity entity)
		{
			_context.Entry(entity).State = EntityState.Modified;
		}

		public TEntity Delete(TEntity entity)
		{
			return _entities.Remove(entity);
		}
	}
}