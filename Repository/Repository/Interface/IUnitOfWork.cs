﻿using DataAccess.Model;
using System;

namespace DataAccess.Interface
{
	public interface IUnitOfWork : IDisposable
	{
		IRepository<TEntity> GetRepository<TEntity>() 
			where TEntity : EntityBase;

		int SaveChanges();
	}
}