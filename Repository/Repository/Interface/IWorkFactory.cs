﻿namespace DataAccess.Interface
{
	public interface IWorkFactory
	{
		IUnitOfWork CreateUnitOfWork();
	}
}