﻿using DataAccess.Model;
using System;

namespace DataAccess.Interface
{
	public interface IRepository<TEntity> 
		where TEntity : EntityBase
	{
		void Create(TEntity entity);
		TEntity Retrieve(Guid id);
		void Update(TEntity entity);
		TEntity Delete(TEntity entity);
	}
}