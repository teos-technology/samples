﻿using DataAccess.Model;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DataAccess.Interface
{
	public interface IStoreContext : IDisposable
	{
		DbSet<User> Users { get; set; }
		DbSet<Product> Products { get; set; }
		DbSet<Customer> Customers { get; set; }
		DbSet<Order> Orders { get; set; }

		DbSet<TEntity> Set<TEntity>() where TEntity : class;
		DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

		int SaveChanges();
	}
}