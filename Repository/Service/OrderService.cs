﻿using DataAccess.Interface;
using DataAccess.Model;
using System;

namespace Service
{
	public class OrderService
	{
		private IWorkFactory _workFactory;

		public OrderService(IWorkFactory workFactory)
		{
			_workFactory = workFactory;
		}

		public Order StartOrder(Guid customerId)
		{
			using (IUnitOfWork work =
				_workFactory.CreateUnitOfWork())
			{
				IRepository<Customer> customers = 
					work.GetRepository<Customer>();

				Order order = new Order
				{
					Id = Guid.NewGuid(),
					Customer =
						customers.Retrieve(customerId)
				};
				
				IRepository<Order> orderRepository = 
					work.GetRepository<Order>();
				orderRepository.Create(order);

				work.SaveChanges();

				return order;
			}
		}
	}
}